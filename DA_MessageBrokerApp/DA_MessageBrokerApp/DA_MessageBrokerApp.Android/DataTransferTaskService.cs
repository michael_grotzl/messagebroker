﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using DA_MessageBrokerApp.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using Xamarin.Forms;

namespace DA_MessageBrokerApp.Droid
{
    [Service]
    class DataTransferTaskService : Service
    {
        static User user = new User { Groups = new List<string>() { "Test","Test2" } };
        CancellationTokenSource _cts;
        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            // From shared code or in your PCL
            //MessagingCenter.Send<object, News>(this, "News", new News() { NewsId = "1", Description = "Test123", Title = "TestTitle", PubDate = new DateTime(100) });
            _cts = new CancellationTokenSource();

            //Task.Run(() =>
            //{
            //    try
            //    {
            var factory = new ConnectionFactory() { HostName = "10.0.0.3", UserName = "test", Password = "test", Port = 5672 };
            IConnection connection = factory.CreateConnection();
            IModel channel = connection.CreateModel();
            var queueName = channel.QueueDeclare().QueueName;

            foreach (string g in user.Groups)
            {
                channel.QueueBind(queue: queueName,
                                  exchange: "Kastner",
                                  routingKey: g);
            }
            channel.ExchangeDeclare(exchange: "Kastner", type: ExchangeType.Direct);

            //var queueName = channel.QueueDeclare().QueueName;

            //foreach (string g in user.Groups)
            //{
            //    channel.QueueBind(queue: queueName,
            //                      exchange: "Kastner",
            //                      routingKey: g);
            //}


            var consumer = new EventingBasicConsumer(channel);

            System.Diagnostics.Debug.WriteLine("vor event");
            consumer.Received += Receive;
            System.Diagnostics.Debug.WriteLine("vor consume");
            channel.BasicConsume(queue: queueName,
                                 autoAck: true,
                                 consumer: consumer);


        
            //    }
            //    catch
            //    {

            //    }
            //    finally
            //    {

            //    }
            //});

            return StartCommandResult.NotSticky;
        }

        void Receive(object sender, BasicDeliverEventArgs ea)
        {
            var body = ea.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);

            var obj = JObject.Parse(message);
            News n;
            Announcement a;
            System.Diagnostics.Debug.WriteLine("vor if");
            if (obj.Properties().Select(p => p.Name).FirstOrDefault() == "NewsId")
            {
                n = JsonConvert.DeserializeObject<News>(message);
                MessagingCenter.Send<object, News>(this, "NewsMessage", n);
            }
            else
            {
                a = JsonConvert.DeserializeObject<Announcement>(message);
                MessagingCenter.Send<object, Announcement>(this, "AnnouncementMessage", a);

            }
        }

        void PS()
        {
            var factory = new ConnectionFactory() { HostName = "10.0.0.3", UserName = "test", Password = "test", Port = 5672 };
            var connection = factory.CreateConnection();
            var channel = connection.CreateModel();
            
                channel.ExchangeDeclare(exchange: "Kastner", type: ExchangeType.Direct);

                var queueName = channel.QueueDeclare().QueueName;

                foreach (string g in user.Groups)
                {
                    channel.QueueBind(queue: queueName,
                                      exchange: "Kastner",
                                      routingKey: g);
                }


                var consumer = new EventingBasicConsumer(channel);

                System.Diagnostics.Debug.WriteLine("vor event");
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body.ToArray();
                    var message = Encoding.UTF8.GetString(body);

                    var obj = JObject.Parse(message);
                    News n;
                    Announcement a;
                    System.Diagnostics.Debug.WriteLine("vor if");
                    if (obj.Properties().Select(p => p.Name).FirstOrDefault() == "NewsId")
                    {
                        n = JsonConvert.DeserializeObject<News>(message);
                        MessagingCenter.Send<object, News>(this, "NewsMessage", n);
                    }
                    else
                    {
                        a = JsonConvert.DeserializeObject<Announcement>(message);
                        MessagingCenter.Send<object, Announcement>(this, "AnnouncementMessage", a);
                        
                    }

                };
                System.Diagnostics.Debug.WriteLine("vor consume");
                channel.BasicConsume(queue: queueName,
                                     autoAck: true,
                                     consumer: consumer);

            
        }
    }
}
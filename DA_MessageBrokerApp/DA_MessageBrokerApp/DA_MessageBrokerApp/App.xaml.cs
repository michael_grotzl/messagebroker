﻿using DA_MessageBrokerApp.Models;
using DA_MessageBrokerApp.Services;
using DA_MessageBrokerApp.Services.Identity;
using DA_MessageBrokerApp.Services.Routing;
using DA_MessageBrokerApp.ViewModels;
using DA_MessageBrokerApp.Views;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DA_MessageBrokerApp
{
    public partial class App : Application
    {
        static User user = new User { Groups = new List<string>() { "News"}
        };
        public App()
        {
            InitializeDi();
            InitializeComponent();

            DependencyService.Register<MockDataStoreNews>();
            DependencyService.Register<MockDataStoreMessages>();
            var isLoogged = Xamarin.Essentials.SecureStorage.GetAsync("isLogged").Result;
            if (isLoogged == "1")
            {
                MainPage = new AppShell();
            }
            else
            {
                MainPage = new LoginPage();
            }
        }
        private void InitializeDi()
        {
            // Services
            Locator.CurrentMutable.RegisterLazySingleton<IRoutingService>(() => new ShellRoutingService());
            Locator.CurrentMutable.RegisterLazySingleton<IIdentityService>(() => new LoginIdentityService());

            // ViewModels
            //Locator.CurrentMutable.Register(() => new LoadingViewModel());
            //Locator.CurrentMutable.Register(() => new LoginViewModel());
        }

        protected override void OnStart()
        {
            //    var factory = new ConnectionFactory() { HostName = "10.0.0.3", UserName = "test", Password = "test", Port = 5672 };
            //    using (var connection = factory.CreateConnection())
            //    using (var channel = connection.CreateModel())
            //    {
            //        channel.ExchangeDeclare(exchange: "Kastner", type: ExchangeType.Direct);

            //        var queueName = channel.QueueDeclare().QueueName;

            //        foreach (string g in user.Groups)
            //        {
            //            channel.QueueBind(queue: queueName,
            //                              exchange: "Kastner",
            //                              routingKey: g);
            //        }



            //        var consumer = new EventingBasicConsumer(channel);

            //        consumer.Received += (model, ea) =>
            //        {
            //            var body = ea.Body.ToArray();
            //            var message = Encoding.UTF8.GetString(body);

            //            var obj = JObject.Parse(message);
            //            News n;
            //            Announcement a;
            //            if (obj.Properties().Select(p => p.Name).FirstOrDefault() == "NewsId")
            //            {
            //                n = JsonConvert.DeserializeObject<News>(message);
            //                MessagingCenter.Send<App, News>(this, "News", n);
            //            }
            //            else
            //            {
            //                a = JsonConvert.DeserializeObject<Announcement>(message);
            //                MessagingCenter.Send<App, Announcement>(this, "News", a);
            //            }

            //        };
            //        channel.BasicConsume(queue: queueName,
            //                             autoAck: true,
            //                             consumer: consumer);

        //}
        }

    protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}

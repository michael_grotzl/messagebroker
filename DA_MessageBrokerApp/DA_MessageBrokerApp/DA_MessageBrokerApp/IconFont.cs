﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DA_MessageBrokerApp
{
	public static class IconFont
	{
		public const string PaperPlane = "\uf1d8";
		public const string CommentDots = "\uf4ad";
		public const string UserCircle = "\uf2bd";
	}
}

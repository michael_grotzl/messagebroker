﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DA_MessageBrokerApp.Models
{
    public class Announcement
    {
        public int AnnouncementId { get; set; }
        public string Title { get; set; }
        public DateTime ExpiringDate { get; set; }
        public string Contents { get; set; }
        public Group Group { get; set; }
        public User Author { get; set; }
    }
}

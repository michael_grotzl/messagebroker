﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DA_MessageBrokerApp.Models
{
    public class Group
    {
        public int GroupId { get; set; }
        public string Name { get; set; }
    }
}

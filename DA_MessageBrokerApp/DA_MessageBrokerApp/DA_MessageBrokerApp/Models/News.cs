﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DA_MessageBrokerApp.Models
{
    public class News
    {
        public int NewsId { get; set; }
        public string Title { get; set; }
        public DateTime PubDate { get; set; }
        public string PicturePath { get; set; }
        public string Description { get; set; }
    }
}

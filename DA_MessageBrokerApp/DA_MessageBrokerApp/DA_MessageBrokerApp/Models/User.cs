﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DA_MessageBrokerApp.Models
{
    public class User
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsWebUser { get; set; }

        //public List<string> Groups { get; set; }

        public User()
        {
            Groups = new List<string>();
            Groups.Add("News");
        }
    }
}

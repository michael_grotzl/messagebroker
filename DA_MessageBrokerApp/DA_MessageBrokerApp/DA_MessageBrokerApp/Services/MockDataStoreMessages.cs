﻿using DA_MessageBrokerApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DA_MessageBrokerApp.Services
{
    public class MockDataStoreMessages : IDataStore<Announcement>
    {
        readonly List<Announcement> messages;

        public MockDataStoreMessages()
        {
            messages = new List<Announcement>()
            {
                new Announcement{ AnnouncementId = Guid.NewGuid().ToString(), Title = "Lorem ipsum dolor sit amet", Contents = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diamLorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam", ExpiringDate = DateTime.Now},
                new Announcement{ AnnouncementId = Guid.NewGuid().ToString(), Title = "Lorem ipsum dolor sit amet", Contents = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam", ExpiringDate = DateTime.Now},
                new Announcement{ AnnouncementId = Guid.NewGuid().ToString(), Title = "Lorem ipsum dolor sit amet", Contents = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam", ExpiringDate = DateTime.Now}
            };
        }

        public async Task<bool> AddItemAsync(Announcement item)
        {
            messages.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(Announcement item)
        {
            var oldItem = messages.Where((Announcement arg) => arg.AnnouncementId == item.AnnouncementId).FirstOrDefault();
            messages.Remove(oldItem);
            messages.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(string id)
        {
            var oldItem = messages.Where((Announcement arg) => arg.AnnouncementId == id).FirstOrDefault();
            messages.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<Announcement> GetItemAsync(string id)
        {
            return await Task.FromResult(messages.FirstOrDefault(s => s.AnnouncementId == id));
        }

        public async Task<IEnumerable<Announcement>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(messages);
        }
    }
}

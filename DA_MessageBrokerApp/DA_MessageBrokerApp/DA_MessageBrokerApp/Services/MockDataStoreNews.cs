﻿using DA_MessageBrokerApp.Models;
using DA_MessageBrokerApp.ViewModels;
using DA_MessageBrokerApp.Views;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DA_MessageBrokerApp.Services
{
    public class MockDataStoreNews : IDataStore<News>
    {
        private List<News> news = new List<News>();
        public MockDataStoreNews()
        {
            //MessagingCenter.Subscribe<object, News>(this, "News", async (sender, arg) =>
            //{
            //    await Task.Run(() => AddItemAsync(arg));

            //});

        }

        public async Task<bool> AddItemAsync(News item)
        {
            news.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(News item)
        {
            var oldItem = news.Where((News arg) => arg.NewsId == item.NewsId).FirstOrDefault();
            news.Remove(oldItem);
            news.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(string id)
        {
            var oldItem = news.Where((News arg) => arg.NewsId == id).FirstOrDefault();
            news.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<News> GetItemAsync(string id)
        {
            return await Task.FromResult(news.FirstOrDefault(s => s.NewsId == id));
        }

        public async Task<IEnumerable<News>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(news);
        }


    }
}

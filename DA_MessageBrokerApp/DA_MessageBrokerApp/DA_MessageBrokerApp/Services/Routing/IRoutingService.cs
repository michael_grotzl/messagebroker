﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DA_MessageBrokerApp.Services.Routing
{
    public interface IRoutingService
    {
        Task GoBack();
        Task GoBackModal();
        Task NavigateTo(string route);
    }
}

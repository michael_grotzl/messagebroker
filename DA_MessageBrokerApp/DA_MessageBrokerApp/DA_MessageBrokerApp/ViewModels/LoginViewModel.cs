﻿using DA_MessageBrokerApp.Models;
using DA_MessageBrokerApp.Services.Routing;
using Newtonsoft.Json;
using Splat;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DA_MessageBrokerApp.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private IRoutingService _navigationService;
        public LoginViewModel(IRoutingService navigationService = null)
        {
            _navigationService = navigationService ?? Locator.Current.GetService<IRoutingService>();
            
            LoginCommand = new Command(async () => await LoginAsync());
        }
        //$2y$10$lUoUOv.zq8wUtMasSo2/z.QXGe09RQr4PVTV9NE9zZokj114pfJGu
        private User user = new User() { Username = "test", Password = "$2y$10$lUoUOv.zq8wUtMasSo2/z.QXGe09RQr4PVTV9NE9zZokj114pfJGu" };
        private bool areCredentialsInvalid;
        private bool isLoginNotCorrect = false;
        public Command LoginCommand { get; set; }

        private async Task LoginAsync()
        {

            var json = JsonConvert.SerializeObject(user);
            var data = new StringContent(json, Encoding.UTF8, "application/json");

            var url = "https://10.0.0.2:5001/infoscreen/login";

            var handler = new HttpClientHandler();
            handler.ClientCertificateOptions = ClientCertificateOption.Manual;
            handler.ServerCertificateCustomValidationCallback =
                (httpRequestMessage, cert, cetChain, policyErrors) =>
                {
                    return true;
                };


            using var client = new HttpClient(handler);

            var response = await client.PostAsync(url, data);

            string result = response.Content.ReadAsStringAsync().Result;
            if(result=="")
            {
                IsLoginNotCorrect = true;
            }
            else 
            { 
                JsonConvert.DeserializeObject<List<string>>(result).ForEach(q => user.Groups.Add(q));
                // This is where you would probably check the login and only if valid do the navigation...
                await _navigationService.NavigateTo("///main");
            }
        }



        public String Username
        {
            get { return user.Username; }
            set { user.Username = value; }
        }

        public String Password
        {
            get { return user.Password; }
            set { user.Password = value; }
        }
        public bool IsLoginNotCorrect
        {
            get => isLoginNotCorrect;
            set
            {
                if (value == isLoginNotCorrect) return;
                isLoginNotCorrect = value;
                OnPropertyChanged();
            }
        }
        public bool AreCredentialsInvalid 
        { 
            get => areCredentialsInvalid;
            set 
            {
                if(value == areCredentialsInvalid) return;
                areCredentialsInvalid = value;
                OnPropertyChanged();
            }
        }


        //private async void Button_Clicked(object sender, EventArgs e)
        //{
        //    MessagingCenter.Send<LoginPage>(this,
        //        (UsernameTxt.Text == "admin") ? "admin" : "user"
        //    );

        //    await Shell.Current.GoToAsync("//main");
        //}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Xamarin.Forms;

namespace DA_MessageBrokerApp.ViewModels
{
    [QueryProperty(nameof(MessageId), nameof(MessageId))]
    public class MessagesDetailViewModel : BaseViewModel
    {
        private string messageid;
        private string text;
        private string description;
        private string videoUrl;
        public string Id { get; set; }

        public string Text
        {
            get => text;
            set => SetProperty(ref text, value);
        }
        public string VideoUrl
        {
            get => videoUrl;
            set => SetProperty(ref videoUrl, value);
        }
        public string Description
        {
            get => description;
            set => SetProperty(ref description, value);
        }

        public string MessageId
        {
            get
            {
                return messageid;
            }
            set
            {
                messageid = value;
                LoadItemId(value);
            }
        }

        public async void LoadItemId(string itemId)
        {
            try
            {
                var item = await DataStoreMessage.GetItemAsync(itemId);
                Id = item.AnnouncementId;
                Text = item.Title;
                Description = item.Contents;
            }
            catch (Exception)
            {
                Debug.WriteLine("Failed to Load Item");
            }
        }
    }
}

﻿using DA_MessageBrokerApp.Models;
using DA_MessageBrokerApp.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DA_MessageBrokerApp.ViewModels
{
    public class MessagesViewModel : BaseViewModel
    {
        private Announcement _selectedItem;

        public ObservableCollection<Announcement> Items { get; }
        public Command LoadItemsCommand { get; }
        public Command AddItemCommand { get; }
        public Command<Announcement> ItemTapped { get; }

        public MessagesViewModel()
        {
            Title = "Eilmeldungen";
            Items = new ObservableCollection<Announcement>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());

            ItemTapped = new Command<Announcement>(OnItemSelected);

        }

        async Task ExecuteLoadItemsCommand()
        {
            IsBusy = true;

            try
            {
                Items.Clear();
                var items = await DataStoreMessage.GetItemsAsync(true);
                foreach (var item in items)
                {
                    Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public void OnAppearing()
        {
            IsBusy = true;
            SelectedItem = null;
        }

        public Announcement SelectedItem
        {
            get => _selectedItem;
            set
            {
                SetProperty(ref _selectedItem, value);
                OnItemSelected(value);
            }
        }


        async void OnItemSelected(Announcement item)
        {
            if (item == null)
                return;

            // This will push the ItemDetailPage onto the navigation stack
            await Shell.Current.GoToAsync($"{nameof(MessagesDetailPage)}?{nameof(MessagesDetailViewModel.MessageId)}={item.AnnouncementId}");
        }
    }
}

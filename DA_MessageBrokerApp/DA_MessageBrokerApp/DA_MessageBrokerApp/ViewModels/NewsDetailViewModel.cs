﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Xamarin.Forms;

namespace DA_MessageBrokerApp.ViewModels
{
    [QueryProperty(nameof(NewsId), nameof(NewsId))]
    public class NewsDetailViewModel : BaseViewModel
    {
        private string newsid;
        private string heading;
        private string newsText;
        private DateTime date;
        public string Id { get; set; }

        public string Heading
        {
            get => heading;
            set => SetProperty(ref heading, value);
        }
        public DateTime Date
        {
            get => date;
            set => SetProperty(ref date, value);
        }

        public string NewsText
        {
            get => newsText;
            set => SetProperty(ref newsText, value);
        }

        public string NewsId
        {
            get
            {
                return newsid;
            }
            set
            {
                newsid = value;
                LoadNewsId(value);
            }
        }

        public async void LoadNewsId(string itemId)
        {
            try
            {
                var item = await DataStoreNews.GetItemAsync(itemId);
                Id = item.NewsId;
                Heading = item.Title;
                NewsText = item.Description;
            }
            catch (Exception)
            {
                Debug.WriteLine("Failed to Load Item");
            }
        }
    }
}

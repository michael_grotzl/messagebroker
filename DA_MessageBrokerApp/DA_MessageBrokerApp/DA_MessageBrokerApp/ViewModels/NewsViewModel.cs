﻿using DA_MessageBrokerApp.Models;
using DA_MessageBrokerApp.Views;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DA_MessageBrokerApp.ViewModels
{
    public class NewsViewModel : BaseViewModel
    {
        static User user = new User { Groups = new List<string>() { "Test","Test2" } };
        private News _selectedItem;

        public ObservableCollection<News> Items { get; }
        public Command LoadItemsCommand { get; }
        public Command AddItemCommand { get; }
        public Command<News> ItemTapped { get; }

        public NewsViewModel()
        {
            Title = "News";
            Items = new ObservableCollection<News>();
            MessagingCenter.Subscribe<object, News>(this, "NewsMessage", async (sender, arg) =>
            {
                await Task.Run(() =>DataStoreNews.AddItemAsync(arg));
                await ExecuteLoadItemsCommand();

            });
            MessagingCenter.Send<NewsViewModel>(this, "StartDataTransferMessage");

            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());
            ItemTapped = new Command<News>(OnItemSelected);
        }

        void AddNewItem(News news)
        {
            if (!Items.Contains(news))
                Items.Add(news);
        }

        async Task ExecuteLoadItemsCommand()
        {
            IsBusy = true;

            try
            {
                Items.Clear();
                var items = await DataStoreNews.GetItemsAsync(true);
                foreach (var item in items)
                {
                    Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public void OnAppearing()
        {
            IsBusy = true;
            SelectedItem = null;
        }

        public News SelectedItem
        {
            get => _selectedItem;
            set
            {
                SetProperty(ref _selectedItem, value);
                OnItemSelected(value);
            }
        }


        async void OnItemSelected(News item)
        {
            if (item == null)
                return;

            // This will push the ItemDetailPage onto the navigation stack
            await Shell.Current.GoToAsync($"{nameof(NewsDetailPage)}?{nameof(NewsDetailViewModel.NewsId)}={item.NewsId}");
        }

        
        
    }
}

﻿using DA_MessageBrokerApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DA_MessageBrokerApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoadingPage : ContentPage
    {
        LoadingViewModel _viewModel;

        public LoadingPage()
        {
            InitializeComponent();
            this.BindingContext = _viewModel = new LoadingViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.Init();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DA_MessageBrokerApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DA_MessageBrokerApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MessagesDetailPage : ContentPage
    {
        public MessagesDetailPage()
        {
            InitializeComponent();
            this.BindingContext = new MessagesDetailViewModel();
        }
    }
}
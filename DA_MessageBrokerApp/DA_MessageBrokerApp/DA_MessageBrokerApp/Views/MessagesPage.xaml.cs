﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DA_MessageBrokerApp.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DA_MessageBrokerApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MessagesPage : ContentPage
    {
        MessagesViewModel _viewModel;
        public MessagesPage()
        {
            InitializeComponent();
            this.BindingContext = _viewModel = new MessagesViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.OnAppearing();
        }
    }
}
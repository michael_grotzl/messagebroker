﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DA_MessageBrokerApp.Models;
using DA_MessageBrokerApp.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DA_MessageBrokerApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewsPage : ContentPage
    {
        NewsViewModel _viewModel;

        public NewsPage()
        {
            InitializeComponent();
            this.BindingContext = _viewModel = new NewsViewModel();
            //MessagingCenter.Send<NewsPage, News>(this, "News", new News() { NewsId = "1", Description = "Test123", Title = "TestTitle", PubDate = new DateTime(100) });
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.OnAppearing();
        }
    }
}